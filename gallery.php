<?php
/**
Template Name: Gallery
 */ ?>
<!doctype html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<?php
	wp_head();
?>
<link href="<?php bloginfo('stylesheet_directory'); ?>/style.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/screen.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/tags.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/mobile.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/css/fontello/fontello.css" media="screen" rel="stylesheet">

<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.bxslider.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>
<!--[if lt IE 9]>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.respond.min.js"></script>
<![endif]-->
<!-- Facebook Conversion Code for facebook checkouts -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6023689139631', {'value':'0.01','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6023689139631&amp;cd[value]=0.01&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
</head>
<body>
<div class="mobile_container" id="full-page">
	<!-- mobile menu -->
	<?php include("mobile_menu.php"); ?>
	<!-- #mobile menu -->

	<div class="body_wrap">
		<div class="swipe-area">
			<a href="#" data-toggle=".container" id="sidebar-toggle">
				<span class="bar"></span>
				<span class="bar"></span>
				<span class="bar"></span>
			</a>
		</div>
		
		<!-- dafault page -->
		<?php include("header.php"); ?>
		<?php include("menu.php"); ?>
		<!-- Main contents -->
		<div id="middle" class="">
		<div class="container clearfix cleartop"> 
		  <!-- Content -->
		  <?php include("breadcrumb.php"); ?>
		  <div class="content-full">
			<div class="post-item boxed post-detail-full">
			  <div class="overview-descr2">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<h4><?php the_title(); ?></h4>
					  <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
					<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
				  <?php endwhile; endif; ?>
			  </div>
			</div>
		  </div>
		</div>
		</div>
		<!-- Footer social -->
		<?php include("footer.php"); ?>
	</div>
		<?php
		wp_footer();
		?>
	</div><!-- #container -->
</body>
</html>
