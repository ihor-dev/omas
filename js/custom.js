jQuery(document).ready(function($) {
	// Remove links outline in IE 7
	$("a").attr("hideFocus", "true").css("outline", "none");

	// Main slider
	$('.main-slider').bxSlider({
		mode: 'fade',
		auto: true,
		adaptiveHeight: true
	});
	// Overview sliders
	$('.overview-slider').bxSlider({
		adaptiveHeight: true,
		pagerCustom: '#overview-pager',
		auto: true,
		mode: 'fade'
	});
	$('#overview-pager').bxSlider({
		slideWidth: 82,
		infiniteLoop: false,
		minSlides: 2,
		maxSlides: 5,
		slideMargin: 4
	});
	


	// Smooth Scroling of ID anchors	
	function filterPath(string) {
	return string
	  .replace(/^\//,'')
	  .replace(/(index|default).[a-zA-Z]{3,4}$/,'')
	  .replace(/\/$/,'');
	}
	var locationPath = filterPath(location.pathname);
	var scrollElem = scrollableElement('html', 'body');
   
	$('a[href*=#].anchor').each(function() {
		$(this).click(function(event) {
			var thisPath = filterPath(this.pathname) || locationPath;
			if (  locationPath == thisPath
			&& (location.hostname == this.hostname || !this.hostname)
			&& this.hash.replace(/#/,'') ) {
				var $target = $(this.hash), target = this.hash;
				if (target && $target.length != 0) {
					var targetOffset = $target.offset().top;
						event.preventDefault();
						$(scrollElem).animate({scrollTop: targetOffset}, 400, function() {
							location.hash = target;
					});
				}
			}
	   });	
	});
 
	// use the first element that is "scrollable"
	function scrollableElement(els) {
		for (var i = 0, argLength = arguments.length; i <argLength; i++) {
			var el = arguments[i],
				$scrollElement = $(el);
			if ($scrollElement.scrollTop()> 0) {
				return el;
			} else {
				$scrollElement.scrollTop(1);
				var isScrollable = $scrollElement.scrollTop()> 0;
				$scrollElement.scrollTop(0);
				if (isScrollable) {
					return el;
				}
			}
		}
		return [];
	}
  
	// prettyPhoto lightbox, check if <a> has atrr data-rel and hide for Mobiles
	if($('a').is('[data-rel]') && screenRes > 600) {
        $('a[data-rel]').each(function() {
			$(this).attr('rel', $(this).data('rel'));
		});
		$("a[rel^='prettyPhoto']").prettyPhoto({social_tools:false});	
    }
    
    
    //mobile menu 
    $(".swipe-area").click(function(){		
		$(".mobile_container").toggleClass("open-sidebar");			
	});
	
	$(".submenu-link").click(function(){
		var id = $(this).attr('menu_id');
		
		$("#menu-"+id).animate({"left":"-240px", "opacity":1},600, function(){
			$("#mobile_menu").hide(400);
		});
	});
	$(".menu_goback").click(function(){
		$("#mobile_menu").show(400);
		$(".submenu").animate({"left":"-680px", "opacity":0},600, function(){
			
		});
	});
	
	$("#searchsubmit").click(function(){
		$("#searchform").submit();
	});
    
    //scroll mobile menu button
    function scrolled(callback) {
		//do by scroll start
		$(this).off('scroll')[0].setTimeout(function(){
			//do by scroll end
			 var top = $(window).scrollTop();
			//$("#sidebar-toggle, #mobile_menu ul, #category_menu ul").animate({'top':top}, 600);
			$("#sidebar-toggle").animate({'top':top}, 600);
			 
			$(this).on('scroll',scrolled);
		}, 500)
	}
	$(window).on('scroll',scrolled);
	
	//hide widgets
	function hide_widgets(){
	var width = $(document).width();
		if(width < 970){
			$("#page div.widget-container:has(.af-form)").hide(); //subscribe
			$("#page div.widget-container:has(.edd-cart)").hide(); //cart
			$("#page div.widget-container:has(.edd-taxonomy-widget)").hide(); //categories		
		}else {
			$("#page div.widget-container").show(1600);
		}	
	}
	hide_widgets();
	$(window).resize(function(){
		hide_widgets();
	});
  
});
