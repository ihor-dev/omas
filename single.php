<?php 
/**
 * @package WordPress
 * @subpackage Default_Theme
 */ ?>
<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title><?php wp_title(''); ?></title>

<?php
	wp_head();
?>
<link href="<?php bloginfo('stylesheet_directory'); ?>/style.css" media="screen" rel="stylesheet">

<link href="<?php bloginfo('stylesheet_directory'); ?>/screen.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/tags.css" media="screen" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.bxslider.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>
<!--[if lt IE 9]>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.respond.min.js"></script>
<![endif]-->
</head>
<body>
<div class="body_wrap">
  <?php include("header.php"); ?>
  <?php include("menu.php"); ?>
  <!-- Main contents -->
  <div id="middle" class="">
    <div class="container clearfix cleartop"> 
      <!-- Content -->
      <?php include("breadcrumb.php"); ?>
      <div class="content">
        <div class="post-item boxed post-detail">
          <div class="overview-descr2">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="navigation">
              <div class="alignleft"><?php previous_post_link('&laquo; %link') ?></div>
              <div class="alignright"><?php next_post_link('%link &raquo;') ?></div>
            </div>
            <h4><?php the_title(); ?></h4>
              <div class="small-text">Posted on: <?php the_time('l, F jS, Y') ?></div>
            <?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>
                <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?><br />
                <?php comments_template(); ?>
            <?php endwhile; else: ?>
            <p>Sorry, no posts matched your criteria.</p>
            <?php endif; ?> 
          </div>
          
          <div class="overview-footer">
            <div class="posted"> Posted in <?php the_category(', ') ?> </div>
            <div class="tagged"> <span></span> <?php the_tags('Tagged In: ', '&nbsp;|&nbsp;', '<br />'); ?> </div>
          </div>
          
        </div>
      </div>
      <!-- Sidebar -->
      <?php include("right-blog.php"); ?>
    </div>
  </div>
  <!-- Footer social -->
  <?php include("footer.php"); ?>
</div>
<?php
	wp_footer();
?>
</body>
</html>
