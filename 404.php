<?php

/**
 * @package WordPress
 * @subpackage Default_Theme
 */ ?>
<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title><?php wp_title(''); ?></title>
<?php
	wp_head();
?>
<link href="<?php bloginfo('stylesheet_directory'); ?>/style.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/screen.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/tags.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/mobile.css" media="screen" rel="stylesheet">

<link href="<?php bloginfo('stylesheet_directory'); ?>/css/fontello/fontello.css" media="screen" rel="stylesheet">

<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.bxslider.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>
<!--[if lt IE 9]>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.respond.min.js"></script>
<![endif]-->
</head>
<body>
<div class="mobile_container" id="page404">
		<!-- mobile menu -->
		<?php include("mobile_menu.php"); ?>
		<!-- #mobile menu -->
		
		<div class="body_wrap">
			<div class="swipe-area">
				<a href="#" data-toggle=".container" id="sidebar-toggle">
					<span class="bar"></span>
					<span class="bar"></span>
					<span class="bar"></span>
				</a>
			</div>
			
			<!-- dafault page -->
			<?php include("header.php"); ?>
			<?php include("menu.php"); ?>
			<!-- Main contents -->
			<div id="middle" class="">
			<div class="container clearfix cleartop"> 
			  <!-- Content -->
			  <?php include("breadcrumb.php"); ?>
			  <div class="content">
				<div class="post-item boxed post-detail">
				  <div class="overview-descr2">
					<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
							<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
							
							  <div class="small-text">Posted on: <?php the_time('l, F jS, Y') ?>&nbsp;&nbsp;In: <?php the_category(', ') ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></div>
						 <p><?php the_content('Read the rest of this entry &raquo;'); ?> </p>
						 <div class="small-text"><?php the_tags('Tagged In: ', '&nbsp;|&nbsp;', '<br />'); ?></div>     
							 <hr /> 
					   <?php endwhile; ?>
					  <?php else : ?>
				<div><h1>Whoops...Seems like the database got lost somewhere...</h1></div>
				<div><p class="center">Sorry, but you are looking for something that isn't here.<br /><br />				<p>		<strong>Don't worry though!</strong> We'll recover it!		</p>		<p>			In the mean time: 		</p>		<ul>			<li>You can probably find our latest designs <a href="http://www.omasplace.com">here</a>.</li>			<li>You can find us on Facebook and see what we're up to <a href="https://www.facebook.com/pages/Omasplace/148171388542211">there</a>.</li>			<li>Lastly, We're on <a href="https://twitter.com/omasplace">Twitter</a> too, so, if they're <em>not</em> having issues, send us a tweet :)</li>		</ul>		<p>			These time wasters together should give me the time to fix this issue! 		</p>	
				<?php get_search_form(); ?></p></div>
				<?php endif; ?> 
				<div class="tf_pagination">
				<?php wp_pagenavi(); ?>
				</div>  
				  </div>
				</div>
			  </div>
			  <!-- Sidebar -->
			  <?php include("right-blog.php"); ?>
			</div>
			</div>
			<!-- Footer social -->
			<?php include("footer.php"); ?>
	</div>
	<?php
		wp_footer();
	?>
	</div> <!-- #mobile_container -->
</body>
</html>
