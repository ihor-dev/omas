<?php
/**
Template Name: Left Sidebar Pages
 */ ?>
<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<?php
	wp_head();
?>
<link href="<?php bloginfo('stylesheet_directory'); ?>/style.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/screen.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/tags.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/mobile.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/css/fontello/fontello.css" media="screen" rel="stylesheet">

<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.bxslider.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>
<!--[if lt IE 9]>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.respond.min.js"></script>
<![endif]-->
</head>
<body>
<div class="mobile_container" id="category-dn">
	<!-- mobile menu -->
	<?php include("mobile_menu.php"); ?>
	<!-- #mobile menu -->
	
	<div class="body_wrap">
		<div class="swipe-area">
			<a href="#" data-toggle=".container" id="sidebar-toggle">
				<span class="bar"></span>
				<span class="bar"></span>
				<span class="bar"></span>
			</a>
		</div>
		
		<!-- dafault page -->
  <?php include("header.php"); ?>
  <?php include("menu.php"); ?>
  <!-- Main contents -->
  <div id="middle" class="">
    <div class="container clearfix cleartop"> 
      <?php include("breadcrumb.php"); ?>
      <!-- Sidebar -->
      <div class="sidebar">
		<?php 
        if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Left Sidebar Pages') ) : ?>
        <?php endif; ?>
      </div>
      <!-- Content -->
      <div class="content2">
        <div class="post-item boxed post-detail">
          <div class="overview-descr2">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <h4><?php the_title(); ?></h4>
                  <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
                <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
              <?php endwhile; endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer social -->
  <?php include("footer.php"); ?>
</div>
<?php
	wp_footer();
?>
</div>
</body>
</html>
