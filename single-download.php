<?php

/**
* @package WordPress
* @subpackage Default_Theme
*/ ?>
<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title><?php wp_title(''); ?></title>
	<?php wp_head();?>
	<link href="<?php bloginfo('stylesheet_directory'); ?>/style.css" media="screen" rel="stylesheet">
	<link href="<?php bloginfo('stylesheet_directory'); ?>/screen.css" media="screen" rel="stylesheet">
	<link href="<?php bloginfo('stylesheet_directory'); ?>/tags.css" media="screen" rel="stylesheet">
	<link href="<?php bloginfo('stylesheet_directory'); ?>/mobile.css" media="screen" rel="stylesheet">

	<link href="<?php bloginfo('stylesheet_directory'); ?>/css/fontello/fontello.css" media="screen" rel="stylesheet">

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.bxslider.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>
	<!--[if lt IE 9]>
		<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/jquery.lightbox.css" />
	<!--[if IE 6]>
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/jquery.lightbox.ie6.css" />
	<![endif]-->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.lightbox.min.js"></script>
	<script type="text/javascript">
	
	jQuery(document).ready(function($){
		$('.lightbox').lightbox();
	});
	</script>
</head>
	<body>
	<div class="mobile_container ">
		<!-- mobile menu -->
		<?php include("mobile_menu.php"); ?>
		<!-- #mobile menu -->
		
		<div class="body_wrap">
			<div class="swipe-area">
				<a href="#" data-toggle=".container" id="sidebar-toggle">
					<span class="bar"></span>
					<span class="bar"></span>
					<span class="bar"></span>
				</a>
			</div>
			
			<!-- dafault page -->
	
		<?php include("header.php"); ?>
		<?php include("menu.php"); ?>
		
		<!-- Main contents -->
		<div id="middle" class="">
		<div class="container clearfix cleartop">
		
		<!-- Content -->
		<?php include("breadcrumb.php"); ?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php $excerpt_length = apply_filters( 'excerpt_length', 40 ); ?>
		
		<div class="content">
		<div class="post-item boxed post-detail">
		<div class="overview-image">
		<!--<div class="price-label label-green">Sale!</div>-->
		<?php echo do_shortcode('[edd_galleries id='.$post->ID.']'); ?>
		<!--<ul class="overview-slider">
		<?php
		global $wpdb;
		$tb_name = $wpdb->prefix . "posts";
		$tb_name2 = $wpdb->prefix . "postmeta";
		$qry = mysql_query("select * from $tb_name where post_parent=".$post->ID." and post_type='attachment' and post_mime_type='image/jpeg'");
		while($row = mysql_fetch_object($qry)){
			$row2 = mysql_fetch_object(mysql_query("select * from $tb_name2 where post_id=".$row->ID." and meta_key='_wp_attached_file'"));
			?>
			<li>
			<?php echo get_the_post_thumbnail($post->ID, "detailimage"); ?>
			</li>
		<?php } ?>
		</ul>
		<div id="overview-pager">
		<?php
		$qry = mysql_query("select * from $tb_name where post_parent=".$post->ID." and post_type='attachment' and post_mime_type='image/jpeg'");
		$count = 1;
		$count2 = 1;
		while($row = mysql_fetch_object($qry)){
			$row2 = mysql_fetch_object(mysql_query("select * from $tb_name2 where post_id=".$row->ID." and meta_key='_wp_attached_file'"));
			?>
			<a data-slide-index="<?php $mycount = $count - $count2; echo $mycount; ?>" href="">
			<?php echo get_the_post_thumbnail($post->ID, "smallimage"); ?>
			</a>
		<?php $count++;} ?>
		</div>-->
		</div>
		<div class="overview-detail">
		<h2><?php the_title(); ?></h2>
		<div class="overview-price">
		<h3><strong><?php edd_price( get_the_ID() ); ?></strong><!-- <span><?php edd_price( get_the_ID() ); ?></span>--></h3>
		</div>
		<?php echo do_shortcode('[edd_social_discount]'); ?>
		<?php echo apply_filters( 'edd_downloads_excerpt', wp_trim_words( get_post_field( 'post_excerpt', get_the_ID() ), $excerpt_length ) ); ?>
		<br>
		<?php echo do_shortcode('[purchase_link id="'.get_the_ID().'" text="Add to Cart" style="button"]'); ?>
		<p style="margin:10px;"><strong>Available Formats</strong> - All designs come in the following embroidery formats: art, dst, exp, hus, jef, pes, shv, vip, vp3, xxx.</p>
		<p style="margin:10px;">Designs will be downloaded in one zip file.  If for some reason your desired format is not found, please contact us.</p>
		</div>
		<div class="overview-descr">
		<h4>Description</h4>
		<?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>
		</div>
		<div class="overview-footer">
		<div class="posted"> Posted in <?php echo get_the_term_list( get_the_ID(), 'download_category', '', ', ', '') ?> </div>
		<div class="tagged"> <span></span> Tagged in <?php echo get_the_term_list( get_the_ID(), 'download_tag', '', ', ', '') ?> </div>
		</div>
		</div>
		<!-- Related items -->
		<div class="related-items">
		<h2 class="container-title">Related Downloads</h2>
		<div class="gallery-list grid_view">
		<?php
		//Get array of terms
		$terms = get_the_terms( $post->ID , 'download_category', 'string');
		//Pluck out the IDs to get an array of IDS
		$term_ids = wp_list_pluck($terms,'term_id');
		//Query posts with tax_query. Choose in 'IN' if want to query posts with any of the terms
		//Chose 'AND' if you want to query for posts with all terms
		$second_query = new WP_Query( array(
		'post_type' => 'download',
		'tax_query' => array(
		array(
		'taxonomy' => 'download_category',
		'field' => 'id',
		'terms' => $term_ids,
		'operator'=> 'IN' //Or 'AND' or 'NOT IN'
		)),
		'posts_per_page' => 3,
		'ignore_sticky_posts' => 1,
		'orderby' => 'DESC',
		'post__not_in'=>array($post->ID)
		) );
		//Loop through posts and display...
		if($second_query->have_posts()) {
			while ($second_query->have_posts() ) : $second_query->the_post();
			@$row2 = mysql_fetch_object(mysql_query("select * from $tb_name2 where post_id=".$post->ID." and meta_key='_thumbnail_id'"));
			@$row3 = mysql_fetch_object(mysql_query("select * from $tb_name2 where post_id=".$row2->meta_value." and meta_key='_wp_attached_file'"));
			?>
			<div class="post-item boxed clearfix">
			<div class="post-image"><a href="<?php the_permalink(); ?>">
			<?php echo get_the_post_thumbnail($post->ID, "mainimage"); ?>
			<span></span></a></div>
			<div class="post-title">
			<div class="post-price"><?php edd_price( get_the_ID() ); ?></div>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			</div>
			</div>
			<?php endwhile; wp_reset_query(); }else{?>
			<p align="center">No Related Download</p>
		<?php } ?>
		</div>
		</div>
		</div>
		<?php endwhile; else: ?>
		<p>Sorry, no posts matched your criteria.</p>
		<?php endif; ?>
		<!-- Sidebar -->
		<?php include("right.php"); ?>
		</div>
		</div>
		<!-- Footer social -->
		<?php include("footer.php"); ?>
	</div>
	<!-- #body wrap -->
	
	<?php
		wp_footer();
	?>
	</div> <!-- #mobile container -->
	</body>
</html>
