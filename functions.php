<?php
	/* Отключаем админ панель для всех пользователей. */
	  //show_admin_bar(false);

// get menu items to mobile menu. CREATE NEW MENU in admin panel width NAME = 'MOBILE'
function get_mobile_categories(){
	if( $menu_items = wp_get_nav_menu_items('mobile') ) { 
		$menu_list = '';
		$submenu = array();
//		print_r($menu_items); die;
		foreach ( (array) $menu_items as $key => $menu_item ) {
			if( $menu_item->menu_item_parent == 0 ){
				$title = $menu_item->title; 
				
				$i = 0;				
				foreach( $menu_items as $k => $val ) {
					if( $val->menu_item_parent == $menu_item->ID) {
						$submenu['menu-'.$menu_item->ID][] = $val;
						$i++;
					}
				}
				$url = $i < 1 ? 'href="' . $menu_item->url . '"' : '';
				
				$class= $i > 0 ? "submenu-link ". $menu_item->attr_title : $menu_item->attr_title;
				$menu_list .= '<li> <a menu_id="'.$menu_item->ID.'" class="'.$class.'" '.$url.' >' . $title . '</a></li>';		
			}
		}
		echo $menu_list;
		return $submenu;
	}
}


add_action('wp_head','nocdn_on_gallery_page');

function nocdn_on_gallery_page() {
	if (is_page('gallery')) { 
		define('DONOTCDN', true); 
	}
}

$themename = "Theme";

$shortname = "proxi";

$options = array (



	array(	"name" => "Theme Options",

			"type" => "title"),

			

	array(	"type" => "open"),

			

		     array(	"name" => "Facebook",

			"desc" => "",

			"id" => $shortname."_facebook",

			"std" => "",

			"type" => "text"),

			

			array(	"name" => "Twitter",

			"desc" => "",

			"id" => $shortname."_twitter",

			"std" => "",

			"type" => "text"),

			

			array(	"name" => "Dribble",

			"desc" => "",

			"id" => $shortname."_dribble",

			"std" => "",

			"type" => "text"),

			

			array(	"name" => "Flickr",

			"desc" => "",

			"id" => $shortname."_flickr",

			"std" => "",

			"type" => "text"),

			

			array(	"name" => "Rss",

			"desc" => "",

			"id" => $shortname."_rss",

			"std" => "",

			"type" => "text"),

			

			array(	"name" => "Logo Title",

			"desc" => "",

			"id" => $shortname."_logotitle",

			"std" => "",

			"type" => "text"),

			

			array(	"name" => "Logo Description",

			"desc" => "",

			"id" => $shortname."_logodata",

			"std" => "",

			"type" => "text"),

			

			array(	"name" => "Footer About Us Title",

			"desc" => "",

			"id" => $shortname."_abouttitle",

			"std" => "",

			"type" => "text"),

			

			array(	"name" => "Footer About Us Content",

			"desc" => "",

			"id" => $shortname."_aboutdata",

			"std" => "",

			"type" => "editor"),

			

array(	"type" => "close")

	

);

function __search_by_title_only( $search, &$wp_query )
{
    global $wpdb;
    if ( empty( $search ) )
        return $search; // skip processing - no search term in query
    $q = $wp_query->query_vars;
    $n = ! empty( $q['exact'] ) ? '' : '%';
    $search =
    $searchand = '';
    foreach ( (array) $q['search_terms'] as $term ) {
        $term = esc_sql( like_escape( $term ) );
        $search .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
        $searchand = ' AND ';
    }
    if ( ! empty( $search ) ) {
        $search = " AND ({$search}) ";
        if ( ! is_user_logged_in() )
            $search .= " AND ($wpdb->posts.post_password = '') ";
    }
    return $search;
}
add_filter( 'posts_search', '__search_by_title_only', 500, 2 );



function mytheme_add_admin() {



    global $themename, $shortname, $options;



    if ( $_GET['page'] == "mytheme_admin" ) {

    

        if ( 'save' == $_REQUEST['action'] ) {



                foreach ($options as $value) {

                    update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }



                foreach ($options as $value) {

                    if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }



                header("Location: admin.php?page=mytheme_admin&saved=true");

                die;



        } else if( 'reset' == $_REQUEST['action'] ) {



            foreach ($options as $value) {

                delete_option( $value['id'] ); }



            header("Location: admin.php?page=mytheme_admin&reset=true");

            die;



        }

    }



   // add_theme_page($themename." Options", "".$themename." Options", 'edit_themes', basename(__FILE__), 'mytheme_admin');

   	

   	

	

}

function mytheme_admin() {



    global $themename, $shortname, $options;



    if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';

    if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';

    

?>

<style type="text/css">

td.bordered { border-bottom:#ccc solid 1px; padding:6px 0;}

</style>

<div class="wrap">

<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">

    <tr>

      <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr align="center" valign="top">

            <td colspan="3" align="left" ><h2><?php echo $themename; ?> Settings</h2></td>

          </tr>

          <tr align="center" valign="top">

            <td colspan="3" class="box1"><form method="post">



<?php foreach ($options as $value) { 

    

	switch ( $value['type'] ) {

	

		case "open":

		?>

        <table width="100%" border="0" cellspacing="0" style="padding:10px;">

		<?php break;



		case 'text':

		?>

        

        <tr>

        

          <td class="bordered" width="30%" valign="middle"><strong><?php echo $value['name']; ?></strong></td>

          <td class="bordered" width="80%"><input style="width:300px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id']) ); } else { echo stripslashes($value['std']); } ?>" /><small><?php echo $value['desc']; ?></small></td>

        </tr>

        <?php 

		break;

		

		case 'textarea':

		?>

        

        <tr>

          <td class="bordered" width="30%" valign="middle"><strong><?php echo $value['name']; ?></strong></td>

          <td width="80%"><textarea  name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" style="width:300px; height:150px;" type="<?php echo $value['type']; ?>" cols="" rows=""><?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id'] )); } else { echo stripslashes($value['std']); } ?></textarea><small><?php echo $value['desc']; ?></small></td>

          

        </tr>

        

        <?php 

		break;

		

		case 'editor':

		?>

        

        <tr>

          <td width="19%" align="left" valign="top"><strong><?php echo $value['name']; ?></strong></td>

          <td width="81%" align="left" valign="top"><?php $content = stripslashes(get_settings( $value['id'] ));

							  $editor_id = $value['id'];

							  wp_editor( $content, $editor_id );?><small><?php echo $value['desc']; ?></small></td>

          

        </tr>



        <?php 

		break;

		

		case 'select':

		?>

        <tr>

          <td width="30%" valign="middle"><strong><?php echo $value['name']; ?></strong></td>

          <td width="80%"><select style="width:300px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"><?php foreach ($value['options'] as $option) { ?><option<?php if ( get_settings( $value['id'] ) == $option) { echo ' selected="selected"'; } elseif ($option == $value['std']) { echo ' selected="selected"'; } ?>><?php echo $option; ?></option><?php } ?></select><small><?php echo $value['desc']; ?></small></td>

        </tr>

                

       <?php

        break;

            

		case "checkbox":

		?>

            <tr>

              <td width="30%" style="padding:6px 0;" valign="middle"><strong><?php echo $value['name']; ?></strong></td>

              <td width="80%"><? if(get_settings($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = ""; } ?>

                <input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />                <small><?php echo $value['desc']; ?></small></td>

            </tr>

                        

            <?php 	break;

 

} 

}

?>

<tr style="margin-top:5px;">

<td align="center" valign="middle" colspan="2"><p><input name="save" type="submit" value="Save changes" class="button-primary" /></p></td>

</tr>

</table>    

<input type="hidden" name="action" value="save" />

</form></td>

          </tr>

        </table></td>

    </tr>

  </table>

  </div>

<?php

}

function Kickass_theme_add_admin() {

			add_menu_page(__('Omas Place'), __('Omas Place'), 'edit_themes', 'mytheme_admin', 'mytheme_admin', get_bloginfo('template_url') . '/images/proxitheme.png', 30);

			add_submenu_page('mytheme_admin', __('Theme Options'), __('Theme Options'), 'edit_themes', 'mytheme_admin', 'mytheme_admin');

}



add_action('admin_menu', 'Kickass_theme_add_admin');

add_action('admin_menu', 'mytheme_add_admin'); ?>
<?php

automatic_feed_links();



if ( function_exists('register_sidebar') ) {

	register_sidebar(array("name" => "Pages",

		'before_widget' => '<div class="widget-container %2$s">

    <div class="widget-ribbon"></div>',

		'after_widget' => '</div>',

		'before_title' => '<h3 class="widget-title">',

		'after_title' => '</h3>',

	));

}



if ( function_exists('register_sidebar') ) {

	register_sidebar(array("name" => "Blog",

		'before_widget' => '<div class="widget-container %2$s">

    <div class="widget-ribbon"></div>',

		'after_widget' => '</div>',

		'before_title' => '<h3 class="widget-title">',

		'after_title' => '</h3>',

	));

}



if ( function_exists('register_sidebar') ) {

	register_sidebar(array("name" => "Left Sidebar Pages",

		'before_widget' => '<div class="widget-container %2$s">

    <div class="widget-ribbon"></div>',

		'after_widget' => '</div>',

		'before_title' => '<h3 class="widget-title">',

		'after_title' => '</h3>',

	));

}



if ( function_exists('register_sidebar') ) {

	register_sidebar(array("name" => "Right Sidebar Pages",

		'before_widget' => '<div class="widget-container %2$s">

    <div class="widget-ribbon"></div>',

		'after_widget' => '</div>',

		'before_title' => '<h3 class="widget-title">',

		'after_title' => '</h3>',

	));

}

add_theme_support( 'post-thumbnails' );

add_image_size( 'mainimage', 190, 190, true );

add_image_size( 'smallimage', 82, 82, true );

add_image_size( 'detailimage', 340, 9999, true );

add_image_size( 'sliderimage', 300, 300, true );

define('EDD_SLUG', 'ed');

function cf7_get_rand($atts){
    return mt_rand();
}
add_shortcode('CF7_get_rand', 'cf7_get_rand');

define( 'EDDSW_SEARCH_LABEL_DISPLAY', false );

