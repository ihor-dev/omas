<?php
@$proxi_logodata = get_option('proxi_logodata');
$carttotal = edd_get_cart_quantity();
?>

<div class="tophead">
  <div class="tophead_lines"></div>
  <div class="tophead_inner">
    <div class="users">
      <div class="cart" id="header-cart">
        <div class="cart-items"><span class="edd-cart-quantity"><?=$carttotal?></span> <?php if($carttotal=="1" or $carttotal=="0"){ ?>item<?php }else{ ?>items<?php } ?></div>
        <div class="cart-amount"><?php echo edd_cart_total(); ?></div>
        <a class="" href="<?php bloginfo('url'); ?>/checkout/">Checkout</a> </div>
      <?php
		if ( is_user_logged_in() ) {
		  get_header('loggedin'); // will include header-loggedin.php
		} else {
		  get_header('notloggedin'); // will include header-notloggedin.php
		}
		?>
    </div>
    <a alt="Machine Embroidery Designs" href="<?php echo get_option('home'); ?>/" class="logo"></a> <strong><a title="Machine Embroidery Designs" alt="Machine Embroidery Designs" href="<?php echo get_option('home'); ?>/">Oma's Place</a></strong>
    <div class="logo_description">Machine Embroidery Designs with a Message</div>
  </div>
</div>
