<div class="header">
  <div class="header_inner">
    <nav id="topmenu" class="clearfix">
      <?php
		  wp_nav_menu( array(
		  'menu'              => 'Top Menu',
		  'container'         => '',
		  'container_class'   => '',
		  'container_id'      => '',
		  'menu_class'        => 'navs',
		  'menu_id'           => '',
		  'echo'              => true,
		  'fallback_cb'       => 'wp_page_menu',
		  'before'            => '',
		  'after'             => '',
		  'link_before'       => '',
		  'link_after'        => '',
		  'depth'             => 0,
		  'theme_location'    => '',
		  'show_home' => true,
		  ) );
	  ?>
	  
    <?php echo do_shortcode('[edd-searchbox class="header-search"]') ?>
    </nav>
    <a class="special_offer" href="<?php bloginfo('url'); ?>/special-offer/" title="Check out Special Offer!"></a> </div>
</div>