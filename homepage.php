<?php
/**
Template Name: Home Page
 */
global $wpdb;
$tb_name = $wpdb->prefix . "posts";
$tb_name2 = $wpdb->prefix . "postmeta";
$tb_name3 = $wpdb->prefix . "term_taxonomy";
$tb_name4 = $wpdb->prefix . "terms";
?>
<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title><?php wp_title(''); ?></title>
<?php
	wp_head();
?>
<link href="<?php bloginfo('stylesheet_directory'); ?>/style.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/screen.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/tags.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/mobile.css" media="screen" rel="stylesheet">

<link href="<?php bloginfo('stylesheet_directory'); ?>/css/fontello/fontello.css" media="screen" rel="stylesheet">

<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.bxslider.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>
<!--[if lt IE 9]>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.respond.min.js"></script>
<![endif]-->
</head>
<body>
	<div class="mobile_container ">
		<!-- mobile menu -->
		<?php include("mobile_menu.php"); ?>
		<!-- #mobile menu -->
		
		<div class="body_wrap">
			<div class="swipe-area">
				<a href="#" data-toggle=".container" id="sidebar-toggle">
					<span class="bar"></span>
					<span class="bar"></span>
					<span class="bar"></span>
				</a>
			</div>
			
			<!-- dafault page -->			
			
		  <?php include("header.php"); ?>
		  <?php include("menu.php"); ?>
		  <!-- Slider -->
		  <div class="slider_wrap">
			<div class="container">
			  <ul class="main-slider">
				<li><center><a href="http://www.omasplace.com/ed/fruit-themed-potholders-2-sizes/"><img src="https://dbk2nsfd8vley.cloudfront.net/wp-content/uploads/2015/05/fruit-themed-potholders.png?c31583"></a></center></li>
				<li><center><a href="http://www.omasplace.com/ed/autism-mug-rug-5x7/"><img src="https://dbk2nsfd8vley.cloudfront.net/wp-content/uploads/2015/05/autism-mug-rug.png?c31583"></a></center></li>
				<li><center><a href="http://www.omasplace.com/ed/category/seasons/spring-seasons/"><img width="970" src="https://dbk2nsfd8vley.cloudfront.net/wp-content/uploads/2015/03/spring-designs-category-banner.jpg"></a></center></li>
				<li><center><a href="http://www.omasplace.com/ed/trendy-flower-purse-for-xl-hoops-ith/"><img src="https://dbk2nsfd8vley.cloudfront.net/wp-content/uploads/2015/03/trendy-purse-xl.png?c31583"></a></center></li>

			  </ul>
			</div>
		  </div>
		  <!-- Main contents -->
		  <div id="middle" class="">
			<div class="container clearfix cleartop"> 
			  <!-- Content -->

			  <div class="gallery-list grid_view">
				<h1>The most recent machine embroidery desgins by Oma:</h1>
				</br>
				 <?php
				if ( get_query_var('paged') ) {
					$paged = get_query_var('paged');
				} else if ( get_query_var('page') ) {
					$paged = get_query_var('page');
				} else {
					$paged = 1;
				}
				query_posts( array( 'post_type' => 'download', 'posts_per_page' => 21, 'orderby' => 'DESC', 'paged' => $paged ) );
				while ( have_posts() ) : the_post();
				@$row2 = mysql_fetch_object(mysql_query("select * from $tb_name2 where post_id=".$post->ID." and meta_key='_thumbnail_id'"));
					@$row3 = mysql_fetch_object(mysql_query("select * from $tb_name2 where post_id=".$row2->meta_value." and meta_key='_wp_attached_file'"));
				?>
				<div class="post-item boxed clearfix"> 
				  <div class="post-image"><a href="<?php the_permalink(); ?>">
				<?php echo get_the_post_thumbnail($post->ID, "mainimage"); ?> 
				<span></span></a>
			  </div>
				  <div class="post-title">
					<div class="post-price"><?php edd_price( get_the_ID() ); ?></div>
					<h2><a title="<?php the_title_attribute(); ?>" itemprop="url" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				  </div>
				</div>
				<?php endwhile; ?>
				<div class="tf_pagination">
				<?php wp_pagenavi(); ?>
				</div>
				<!-- Pagination -->
				<!--<div class="tf_pagination"> <a href="#" class=""><span>Previous</span></a> <a href="#" class="active"><span>1</span></a> <a href="#" class=""><span>2</span></a> <a href="#" class=""><span>3</span></a> <a href="#" class=""><span>4</span></a> <a href="#" class=""><span>5</span></a> <a href="#" class=""><span>Next</span></a> </div>-->
			  </div>

			  <!-- Sidebar -->
			  <?php include("right.php"); ?>
			</div>
		  </div>
		  <!-- footer social -->
		  <?php include("footer.php"); ?>
		</div>
	<?php
		wp_footer();
	?>
	</div> <!-- end container -->
</body>
</html>
