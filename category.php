<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */ ?>
<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title><?php wp_title(''); ?></title>
<?php
	wp_head();
?>
<link href="<?php bloginfo('stylesheet_directory'); ?>/style.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/screen.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/tags.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/mobile.css" media="screen" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/css/fontello/fontello.css" media="screen" rel="stylesheet">

<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.bxslider.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>
<!--[if lt IE 9]>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.respond.min.js"></script>
<![endif]-->
</head>
<body>
<div class="mobile_container" id="category-dn">
	<!-- mobile menu -->
	<?php include("mobile_menu.php"); ?>
	<!-- #mobile menu -->
	
	<div class="body_wrap">
		<div class="swipe-area">
			<a href="#" data-toggle=".container" id="sidebar-toggle">
				<span class="bar"></span>
				<span class="bar"></span>
				<span class="bar"></span>
			</a>
		</div>
		
		<!-- dafault page -->
  <?php include("header.php"); ?>
  <?php include("menu.php"); ?>
  <!-- Main contents -->
  <div id="middle" class="">
    <div class="container clearfix cleartop"> 
      <!-- Content -->
      <?php include("breadcrumb.php"); ?>
      <div class="content">
        
        <div class="related-items">
          <div class="gallery-list grid_view">
			
            <?php 
			global $wpdb;
			$tb_name = $wpdb->prefix . "posts";
			$tb_name2 = $wpdb->prefix . "postmeta";
			if (have_posts()) : ?>
        	<?php while (have_posts()) : the_post();
			$row2 = mysql_fetch_object(mysql_query("select * from $tb_name2 where post_id=".$post->ID." and meta_key='_thumbnail_id'"));
			$row3 = mysql_fetch_object(mysql_query("select * from $tb_name2 where post_id=".$row2->meta_value." and meta_key='_wp_attached_file'"));
			?>
            <div class="post-item boxed clearfix">
              <div class="post-image">
			<a href="<?php the_permalink(); ?>">
				<?php echo get_the_post_thumbnail($post->ID, "mainimage"); ?> 
				<span></span>
			</a>
		</div>
              <div class="post-title">
                <div class="post-price"><?php edd_price( get_the_ID() ); ?></div>
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
              </div>
            </div>
            <?php endwhile; ?>
        <?php else :endif; ?>
            <div class="tf_pagination">
        <?php wp_pagenavi(); ?>
        </div>
          </div>
        </div>
        
        
      </div>
      <!-- Sidebar -->
      <?php include("right.php"); ?>
    </div>
  </div>
  <!-- Footer social -->
  <?php include("footer.php"); ?>
</div>
<?php
	wp_footer();
?>
</div>
</body>
</html>
