<div id="mobile_menu" class="pchide">
	<ul>		
		<li><a class="icon-home" href="<?php bloginfo('url'); ?>">Home</a></li>		
		<li>
			<form action="<?php bloginfo('url'); ?>" class="searchform eddsw-search-form" id="searchform" method="get" role="search">
				<div class="eddsw-form-container">
					<input type="hidden" value="download" name="post_type">
					<input type="text" placeholder="Type search…" class="s eddsw-search-field" id="s" name="s" value="">
					<span id="searchsubmit" ><span class="icon-search"></span></span>
					
				</div>
			</form>
		</li>
		<? $submenu = get_mobile_categories();?>
		<? if ( is_user_logged_in() ):?>
			<li><a class="icon-money" href="<?php bloginfo('url'); ?>/checkout/point-balance/">Point Balance</a></li>
			<li><a class="icon-history" href="<?php bloginfo('url'); ?>/checkout/purchase-history/">Purchase History</a></li>
			<li><a class="icon-user" href="<?php bloginfo('url'); ?>/your-account/">Your Account</a></li>
			<li><a class="icon-star-empty" href="<?php bloginfo('url'); ?>/wish-lists/">Wish Lists</a></li>
			<li><a class="icon-off" href="<?php echo wp_logout_url('$index.php'); ?>">Logout</a></li>
		<?else:?>
			<li><a class="icon-user-plus" href="<?php bloginfo('url'); ?>/register/">Register</a></li>
			<li><a class="icon-lock" href="<?php bloginfo('url'); ?>/wp-login.php?action=lostpassword">Lost Password</a></li>
			<li><a class="icon-key" href="<?php bloginfo('url'); ?>/login/">Login</a></li>
		<? endif;?>
  </ul>
</div>

<?php foreach($submenu as $key=>$val):?>
<div id="<?=$key;?>" class="submenu pchide">
	<ul>
		<li><a class="menu_goback icon-reply-all" >back</a></li>
		<?foreach($val as $sub):?>
			<li><a href="<?=$sub->url;?>" class="<?=$sub->attr_title;?>" ><?=$sub->title;?></a></li>
		<?endforeach; ?>
	</ul>
</div>
<? endforeach;?>

  

	
