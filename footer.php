<?php
@$proxi_facebook = get_option('proxi_facebook');
@$proxi_twitter = get_option('proxi_twitter');
@$proxi_dribble = get_option('proxi_dribble');
@$proxi_flickr = get_option('proxi_flickr');
@$proxi_rss = get_option('proxi_rss');
@$proxi_abouttitle = get_option('proxi_abouttitle');
@$proxi_aboutdata = get_option('proxi_aboutdata');
?>
<div class="footer_social">
  <div class="footer_social_inner">
    <div class="container"> <a href="<?=$proxi_facebook?>" target="_blank" class="social-fb">Facebook</a> <a href="<?=$proxi_twitter?>" target="_blank" class="social-twitter">Twitter</a> <a href="<?=$proxi_rss?>" target="_blank" class="social-rss">RSS Feed</a> </div>
  </div>
</div>
<div class="footer">
  <div class="container">
    <div class="grid_4 mhide">
      <h3><?=stripslashes($proxi_abouttitle)?></h3>
      <p><?=stripslashes($proxi_aboutdata)?></p>
    </div>
    <div class="grid_2 mhide" >
      <h3>Quick Links</h3>
      <?php
              wp_nav_menu( array(
              'menu'              => 'Footer Resources Menu',
              'container'         => '',
              'container_class'   => '',
              'container_id'      => '',
              'menu_class'        => '',
              'menu_id'           => '',
              'echo'              => true,
              'fallback_cb'       => 'wp_page_menu',
              'before'            => '',
              'after'             => '',
              'link_before'       => '',
              'link_after'        => '',
              'depth'             => 0,
              'theme_location'    => '',
              'show_home' => true,
              ) );
          ?>
    </div>
    <div class="grid_2 support">
      <h3>Support</h3>
      <?php
              wp_nav_menu( array(
              'menu'              => 'Footer Supports Menu',
              'container'         => '',
              'container_class'   => '',
              'container_id'      => '',
              'menu_class'        => '',
              'menu_id'           => '',
              'echo'              => true,
              'fallback_cb'       => 'wp_page_menu',
              'before'            => '',
              'after'             => '',
              'link_before'       => '',
              'link_after'        => '',
              'depth'             => 0,
              'theme_location'    => '',
              'show_home' => true,
              ) );
          ?>
    </div>
    <div class="grid_4 subscribe">
      <h3>Newsletter Sign-up</h3>
      <p>Join our newsletter to get latest updates, Special Offer, Tips &amp; Tutorials right in your inbox.</p>
      
<form method="post" class="af-form-wrapper" action="https://www.aweber.com/scripts/addlead.pl"  target="_blank" class="newsletter">
<input type="hidden" name="meta_web_form_id" value="2017067368" />
<input type="hidden" name="meta_split_id" value="" />
<input type="hidden" name="listname" value="omasplace" />
<input type="hidden" name="redirect" value="https://www.aweber.com/thankyou-coi.htm?m=text" id="redirect_8c645b0513ee0a3421f1106358a0bb6a" />
<input type="hidden" name="meta_adtracking" value="Omasplace-Footer" />
<input type="hidden" name="meta_message" value="1" />
<input type="hidden" name="meta_required" value="name,email" />
<input type="hidden" name="meta_tooltip" value="" />

<input id="awf_field-57703730" type="text" name="name" class="text" value=""  tabindex="500" placeholder="Name"/>
<input class="text" id="awf_field-57703731" type="text" name="email" value="" tabindex="501" placeholder="Email address"  />
<input type="submit" name="submit" class="button"  value="Submit" tabindex="502" />

</form>
      
      <div class="footer_rss">Subscribe also to our <a href="<?=$proxi_rss?>" target="_blank">RSS feed</a></div>
    </div>
  </div>
  <div class="container">
    <div class="copyright">
      <p><?php echo date("Y"); ?> &copy; <a href="<?php echo get_option('home'); ?>/">Oma's Place.com</a><span> All Rights Reserved.</span></p>
      <!--<p class="right">EDD Digital Store by <a href="#">Easy Digital Downloads</a>.</p>-->
    </div>
  </div>
  <div class="footer_lines"></div>
</div>
